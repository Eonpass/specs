# Eonpass

![Eonpass](assets/logo.jpg)

Eonpass is the open source protocol to handle shipment data. It was originally designed by the infamous team Cryptomice at EU Blockathon 2018 where it won the final prize. In 2023 we realized the official [Proof of Concept](https://www.youtube.com/watch?v=aQKnazbrl84) with four important brands importing into the EU through the Dutch Customs.

## Brief Summary
Fake goods are injected in the markets everyday, they cause loss of sales for legitimate intellectual property holders, dilute brand equity and potentally create hazard for the consumers. In this context Eonpass protocol was designed to create a virtual world where goods are checked before the real world. 

## Document Structure
The document is organized as follows:

- [Architecture](https://gitlab.com/Eonpass/specs/blob/master/architecture.md)
- [Eonpeers: Logistics Node Reference Implementation](https://gitlab.com/Eonpass/eonpeers)
- [Eonbasics: Timestamping on Liquid Elements and EBSI Reference Implementation](https://gitlab.com/Eonpass/eonbasics)
- [ELIPs: ELSA Improvement Proposals](elips/REAMDE.MD)
- [Special Thanks](#)