# Protocol Architecture

In this section we describe the functions that compose the protocol. Eonpass is focused on the logistics part of the protocol but here we give also an overview of the anti-counterfeiting labels and brand digital certificates. In <mark style="background-color: lightblue">highlight</mark> the parts that are addressed by Eonpass peer to peer logistics tools.

## Actors

1. _Brand Owners_, IP holders are the registered owners of a brand within EUIPO or other insitituions. 
2. _Producers_, they represent factories that actually assemble/build the products for a brand, they can be independent or controlled by Brand Owners. 
3. _Logistics Operators_, Freight Forwarders and Carriers moving the goods between places. They must assess the authenticity of goods and shipment requests. Once they take a shipment in charge they are liable for eventual violations.
4. _Customs_, they perform Customs duties, pre-arrival checks and even physical inspections on goods entering/exiting a country.
5. _Distribution_, Set of warehouses and shops where the goods are stored waiting for ecommerce orders or displayed for the consumers. They must assess the origin of the received products and eventually let consumers check that origin too.
6. _Consumers_, General public, they may want to check the authenticity of goods or may be required to prove authenticity to access customer service, reparis, insurances, etc.
7. _Customer Service, Insurance and other services_, Business entities handling consumers requests or selling additional services like insurances. They must assess that the product subject of the request is authentic.

![Actors](assets/actors.jpg)

## Process

- The _Brand Owner_ requries the **Digital Ceritificate** (Verifiable Credential) of one of its Brands to the EUIPO (in the future also to other patent offices)
  - this digital certificate is linked to a **Key**, so that the Brand owner can sign messages with the same **Key**. Anyone can check that messages are signed by the specific entity who received the **Digital Certificate** from the EUIPO.
- The _Brand Owner_ creates digital representation of goods (for instance creating a batch of NFTs, Digital Passports or other implementations) and to each adds a signed message called **Anti-counterfeiting Metadata Label** with:
  - general characteristics of the specific item
  - general distribution details (expected distribution year, expected geographics areas)
  - a reference to the the brand digital certificate emitted by the EUIPO
- <mark style="background-color: lightblue">The _Brand Owner_ creates a **Shipment Request** for the new products and signs it</mark>
- [Optional] The _Producer_ can check the signed messages for each good and for the shipment details

<hr/>

- <mark style="background-color: lightblue">The _Logistics Operator_ receives the **Shipment Request** composed of:</mark>
  - <mark style="background-color: lightblue">a **Shipment** description with content details (pieces, products, total weight, height, etc..)</mark>
  - <mark style="background-color: lightblue">at least a **Transport Segment** starting from the desired origin and ending in the desired destination</mark>
  - <mark style="background-color: lightblue">a **Quote Request** with reference to eventual long term contracts or other commercial information</mark>
  - <mark style="background-color: lightblue">the overall signature from the _Brand Owner_ covering every data received (Quote, Transport Segment, Shipment)</mark>
  - <mark style="background-color: lightblue">a specific _Brand Owner_ signature only on the shipment contents</mark>
- <mark style="background-color: lightblue"> The _Logistcs Operator_ evaluates the quote requests and eventually breaks down the itinerary and asks for quotes to other _Logistics Operators_. A classic example: Freight Forwarder breaking down the itinerary in: Factory->Port, Port->Port, Port->Warehouse, where the Port->Port part of the trip is managed by a _Carrier_.</mark>
  - <mark style="background-color: lightblue">When a _Logistics Operator_ asks for quotes to subcontractors, it can either pass the original shipment description with the specific _Brand Owner_ signature or create a completely new shipment description. These two options will have different legal, commercial and fiscal advantages. The first one, full disclosure, will be useful for faster clearance, faster IP protection, faster trust building between new partners (since they can prove there's an approved _Brand Owner_ behind the shipment). The second one maintains full commercial secrecy, the _Logistic Operator_ is not disclosing who exactly is requesting the shipment, but doesn't offer the other advantges.</mark>
- <mark style="background-color: lightblue">When the _Logistics Operator_ has a clear picture of the prices and scheduling it replies to the _Brand Owner_ with an **Offer** containing the commercial details: prices, scheduled times, other commercial conditions.</mark>
- <mark style="background-color: lightblue">The _Brand Owner_ can accept the offer which triggers the creation of a **Booking**, which is basically the confirmation of the **Offer** details</mark>
- <mark style="background-color: lightblue">When the shipment is in execution, the goods are travelling, the _Logistics Operator_ can add **Events** to the shipment, keeping track of its history</mark>

<hr/>

- <mark style="background-color: lightblue">_Customs_ can request view-only access to any shipment, they'll be able to see the shipment details and its related events </mark>
  - <mark style="background-color: lightblue">_Customs_ can see shipments way before than their actual execution, this will allow _Customs_ to perform statistical analysis or pre-arrival checks</mark>
- _Customs_ can also perform physical inspection of a shipment, meaning that they'll open it and can touch the single items inside. In this scenario _Customs_ will be able to inspect also the labels and the linked **Anti-counterfeiting Metadata**


<hr/>

- _Consumers_ via a dedicated protal will be able to check the label, get the **Anti-counterfeiting Metadata** and see if its information match the information of the product they have in hands. 
- if NFTs were chosen, _Consumers_ can also receive the token on their own wallet. The token or other digital representations always hold a reference to the **Anti-counterfeiting Metadata**
- _Customer Service, Insurances, Warranties and others_ may inspect the virtual tokens before granting their services.

## Definitions

Useful definitions that are relevant to understand the overall processes and architecture.

### Key
A private/public keypair for secpt256k1. 
  - it will be used by _Brand Owners_ to request **Digital Certificates** and then to sign **Anti-counterfeiting Metadata Labels** and **Shipment Requests**.
  - also _Logistic Operators_ will use a key to sign their messages (quote requests, offers and bookings)

Eventually this key can also be used to authenticate with special timestamping services, offered for instance by EBSI. In fact actors in the logistics network can request notarisation for the shipment data they send and receive as additional proof to be provided during _Customs_ checks for instance.

### Wallet
Software that manages keys. A wallet can be a standalone software or be embedded in other tools. Eonpass node is able to manage keys, sign messages and eventually even broadcast transactions to specific blockchain networks, it can act as a Wallet.

In the context of the overall process, _Brand Owners_ will also have a dedicated software called _Brand Wallet_ which will be responsible to requeset **Digital Certificates** for their brands to the EUIPO. This _Brand Wallet_ is not interacting with the Logistics process, but the same actor will use the same key in the two wallets (or even better: the two wallets will have access to the same key inside a KMS).

### Signature
A signature is a mathematical scheme to prove that an actor signed a message. The public key is shared and used to prove that only the owner of the private key could have signed that message. We use the standard ES256K type of signature.

### Logistics Node
Brand Owners, Customs and Logistics Operators are part of a peer to peer network to exchange information about shipments. A node in this network is capable of generating, signing, sending and receiving messages describing: Quote Requests, Shipment, Offers, Shipment Events and Bookings. The visiblity of those objects on the network follows this general pattern:
  - The requester and the provider of a quote request and offer can see the quote request, the offer and everything connected to the shipment
  - External parties, like Customs, can request view-only access to shipments and will never see quote requests and offers.
  - If someone is not the requester or the provider and is not given view-only access to a shipment, it can't see anything of that shipment

### Logistics Network
The node communicate with eachother, creating the network.

### Digital Certificate: Verifiable Credential
A Verifiable Credentials is a signed message which has three components: 
  - a header describing the type of signature and a reference to which key is signing, 
  - a payload or body which contains assertions about a topic and about who is signing and eventually who is receiving this verifiable credential
  - a footer with the signature

Verifiable Credentials in our context will be issued by the EUIPO to Brand Owners. Each credential attests that the Brand Owner is indeed owning a specified Trademark according to the EUIPO Registry. The EUIPO will use EBSI as architecture to host references to keys and identifiers.

### Token
_Brand Owners_ may decide to use NFT (Non-Fungible Tokens) to represent their goods. A token is a virtual object that is owned by a key, meaning that only key-owner can send it to someone else. The ownership of tokens is stored and managed on a blockchain. Usually wallets also offer functions to handle tokens. 

### Token Metadata
Regardless of the technology chosen to represent a virtual object, virtual object have additional information other than "who is their owner". Such additional information, like physical details, URLs, etc.., is usually stored in a dedicated file called "metadata". The blockchain only knows the location (URL) of the metadata and sometimes a compact version of the content. The metadata can be stored on classic web servers and IPFS. 

## Logistics Data Structures

The objects within the Logistics P2P network is inspired by IATA One Record, with some extensions to capture the peer to peer exchanges and the possible notarization of data.

### Product
A product describes at least a product family identified by an [HS code](https://taxation-customs.ec.europa.eu/customs-4/calculation-customs-duties/customs-tariff/harmonized-system-general-information_en).

### Piece
The logstics network is "piece-centric", each pallet or box inside a shipment can be identified with a piece. Pieces are hierarchical so that a piece can have a "parent piece" and describe complex structures. There is at the moment no fixed rule, you can choose to describe everything as a single piece or to break down the content in pallets and boxes.

### Shipment
A shipment is a container of pieces, it has overall measurements summing up all the contained pieces (e.g. total weight, total height, etc..)

### Transport Segment
A segment has two location: one departure and one arrival, and one desired departure date. One or more segments can be connected to a shipment.

### Quote Request
The Quote Request contains commercial information about a requested shipment. Quote Requests are sent from _Brand Owners_ to their _Freight Forwarders_ for instance, or sent from _Freight Forwarders_ to _Carriers_.

### Offer
One or more Offers can be linked to a Quote Request. Eventually the Quote Request requester can accept one of the received Offers.

### Booking
Once an offer is accepted, the shipment provider creates a Booking object which contains the confirmation for the booked trip.

### Event
At any point in time during the shipment execution, the _Logistic Operator_ can upload Events describing what's happening to the shipment: departure, arrival, in transit information and also sensor readings. This is a simple object insipired by the GS1 EPCIS standard, it contains the minimum information to answer: what, where, when, why. 

### Disclosure
This object keeps track of who can access which shipment in a node.

### Company
A company is a business entity identified by VAT and country. Each logistics node has the list of companies it makes business with. Each node also has an "administrator company", which means that it's the company managing the node. Nodes use this information to know to who send quote requests and from who expect offers.

### User
A node can be accessed by its administrator user, the administrator can see any information inside its own node. Other users can be created, for instance for Customs, which don't see anything inside the node unless they have a disclosure.

## Processes of the Logistics Network

### Company Handshake
![Handshake](assets/Diagrams_ELSA_2023-CompanyHandshake.jpg)

### Quote Request and Offer life cycle
![QuoteRequest](assets/Diagrams_ELSA_2023-QuoteRequest.jpg)

![Offer](assets/Diagrams_ELSA_2023-Offer.jpg)

![Booking](assets/Diagrams_ELSA_2023-Booking.jpg)

### Verification
![Verification](assets/Diagrams_ELSA_2023-Verification.jpg)

### Shipment Notarization
![Notarization](assets/Diagrams_ELSA_2023-Notarization.jpg)

### Adding and Sharing Events
WIP

### Commercial-data Visibility
![Vsibility1](assets/Diagrams_ELSA_2023-CommercialDataVisibility.jpg)

![Vsibility2](assets/Diagrams_ELSA_2023-CommercialDataVisibility2.jpg)




