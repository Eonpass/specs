ELIP-01
=======

Companies
---------

`draft` `mandatory`

This ELIP defines the basic structure of a company object that should be implemented by everybody. New ELIPs may add new optional (or mandatory) fields and messages and features to the structure described here.

## Company

Each client, or _node_, has a database where it stores objects. Companies are the basic structure used to describe brand owners, logistics operators or other businesses involved in the EBSI-ELSA network.


The `company` object type has the following format on the wire, meaning when it's exchanged between nodes or returned from an API query:

```
{
  "public_id": <string uuid.uuid4(), identifier of this company on the network>,
  "public_key": <string, pem-encoded public key, key used for signatures by this company>,
  "did": <string, did string like did:ebsi:z1....1>,
  "name": <string, name of the company>,
  "vat_number": <string, VAT or EIN number of the company, we assume this is unique in the same country of fiscal residence>,
  "fiscal_address": <string, street address of the fiscal residence of the company>,
  "fiscal_city": <string, city of the fiscal residence of the company>,
  "fiscal_province":<string, province of the fiscal residence of the company>,
  "fiscal_country": <string, country of the fiscal residence of the company>,
  "fiscal_zip": <string, zip code of the fiscal residence of the company>,
  "unique_hash": <string, hash of the string concatenation of VAT number (or EIN number) and fiscal country >,
  "eori_number": <string, company eori identification>,
  "aeo_status": <string, aeo status of the company>,
  "base_url": <string, url of the node owned by this company>
}
```

To obtain the `unique_hash`, we `sha256` the serialized concatenation of `vat_number` and `fiscal_country`. Relationship in the database should always reference the public_id. Inside a _node_ `vat_number` and `fiscal_country` should also be a unique constraint.

See following chapter to see how companies exchange their information among each other.


### Node Owner

Each _node_ in the EBSI-ELSA network is owned by a company. A _node_ will also store in its database many other companies, therefore it's important to create a special mark and special function to identify the company which owns the _node_.

Companies in the database will have a Boolean field `is_own`, only one company at a time can have this field set to `true` within a _node_. There are specific messages exchanged about the node owner. See following chapter.

### Database Structure

In this section we provide a sample implementation with `SQLAlchemy` of the minimum desired table for a `company` with fields and their respective characteristics:

  - unique, must appear only once
  - nullable, can be left empty

```python
    public_id = db.Column(db.String(100), unique=True, primary_key=True,)
    name = db.Column(db.String(255), unique=True, nullable=False)
    vat_number = db.Column(db.String(255), unique=True, nullable=False)
    created_on = db.Column(db.DateTime, nullable=False)
    is_own = db.Column(db.Boolean, nullable=False, default=False)
    did = db.Column(db.String(1000), unique=False, nullable=True)
    base_url = db.Column(db.String(255))
    public_key = db.Column(db.String(255), unique=False, nullable=True)
    fiscal_address = db.Column(db.String(255), unique=False, nullable=True)
    fiscal_city = db.Column(db.String(255), unique=False, nullable=True)
    fiscal_province = db.Column(db.String(255), unique=False, nullable=True)
    fiscal_country = db.Column(db.String(255), unique=False, nullable=False)
    fiscal_zip = db.Column(db.String(255), unique=False, nullable=True)
    eori_number = db.Column(db.String(255), unique=False, nullable=True)
    aeo_status = db.Column(db.String(255), unique=False, nullable=True)
    unique_hash = db.Column(db.String(255), unique=True, nullable=False, index=True)    
    latest_log = db.Column(db.String(255), unique=False, nullable=True)
    __table_args__ = (db.UniqueConstraint('vat_number', 'fiscal_country', name='_company_vat_country_uc'),)

```

`latest_log` is a quality-of-life field. Since _nodes_ exchange information, the back and forth of requests should be logged, nonetheless it's quick and simple to just store the latest result of a message exchange within the object. This will make debugging easier also for users not seeing the log resources.

### Node Owner and the node Key

A _node_ must also have access to a private Key. Infact _nodes_ are requried to sign payloads and declare a `public_key` which represents them.

Therefore when a _node_ `GET /node-owner` is invoked, the _node_ will check its private key, compute or retrieve the related public key and populate the `public_key` field in the payload.

For all other companies which are not the node-owner, the _node_ will just receive and save a public key.

## Communication between nodes about companies

Nodes must expose the following API endpoint, expressed from their `base_url`: 

  - `GET /node-owner`, not authenticated, eventually _nodes_ will implement IP filters or firewalls or throttling strategies to block spammers

This endpoint must return a company payload. The returned company must be the _node owner_, i.e. the company that has `is_own = True` in that node. If there is no company with `is_own = True`, then the endpoint will return error 404, resource not found. This endpoint should be open because it will be used by nodes who want to add your company to their list of companies.

### How to use /node-owner to create companies in a node

When _node A_ wants to add the owner of _node B_ to its list of companies, it will use `GET node_b_base_url/node-owner` to get _node B_ owner's details.

  * How company representatives exhange their `base_url` is out of scope. This can be done via private channels or by setting up a directory representing a consortium.
  * In the pilot with the EUIPO there was the following automation:
    * Whenever a new company is created or updated, if `base_url` is present, `GET base_url/note-owner` is called
    * if there's a company payload in the response, then extract `vat_number` and `fiscal_country` and search the local database for a match
      * if there's a match, remove the match and create the company with the given company payload. 
      * If the new or updated company had different `vat_number` and `fiscal_country`, remove it and create the company with the given payload
    * TODO: since a `public_key` is present, the node-owner should respond with a signed company payload structured as a Verifiable Credential
    * TODO: if did is present, then the _node_ should retrieve the public key from the did provider (or extract it if it's a did:key)
  * In the 2024 UAT edidtion of the protocol, _node_ owners had also the choice to accept or deny incoming connection requests
    * TODO: describe the process of a "connection request"

### Impersonation

Since anyone can spin up a _node_ and create a public key, it's important to be conscious about possible impersonators:

  * The network is open but only company representatives that know each other should connect their nodes
  * Some did providers, like EBSI, make sure there exist a trust chain before creating a did
  * As a good practice for the first data exchanges between nodes, it would be best for the respectie owners to setup a conference call and check in real-time to make sure their nodes are properly connected





