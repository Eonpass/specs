# ELIPs

ELIPs stand for **EBSI-ELSA Implementation Possibilities**.

They exist to document what may be implemented by [EBSI-ELSA](https://www.euipo.europa.eu/en/observatory/enforcement/blockathon)-compatible _node_ software.

A _nodes_ is an API server with its own database. ELIPs describe what objects should be present in the database and what are the minimum messages that _nodes_ can exchange with each others.

---

- [List](#list)
- [Object Kinds](#object-kinds)
- [Message Types](#message-types)
  - [Node to Node](#node-to-node)
- [Criteria for acceptance of ELIPs](#criteria-for-acceptance-of-elips)
- [Is this repository a centralizing factor?](#is-this-repository-a-centralizing-factor)
- [How this repository works](#how-this-repository-works)
- [Breaking Changes](#breaking-changes)
- [License](#license)

---

## List

- [ELIP-01: Companies and Nodes](01.md)
- [ELIP-02: Users and Disclosures](02.md)
- [ELIP-03: OpenTimestamps Attestations for Events](03.md)



## Object Kinds
| kind          | description                | ELIP                     |
| ------------- | -------------------------- | ------------------------ |
| `0`           | Companies                  | [01](01.md)              |
| `1`           | Users                      | [02](01.md)              |
| `2`           | Disclosure                 | [02](02.md)              |


## Message types

### Node to Node

| type         | description                                         | ELIP        |
| ------------ | --------------------------------------------------- | ----------- |
| `NODE_OWNER` | get information of the company running the node     | [01](01.md) |

Please update these lists when proposing new ELIPs.

## Criteria for acceptance of ELIPs

1. They should be fully implemented in at least two clients -- when applicable.
2. They should make sense.
3. They should be optional and backwards-compatible: care must be taken such that clients that choose to not implement them do not stop working when interacting with the ones that choose to.
4. There should be no more than one way of doing the same thing.
5. The full EBSI-ELSA steering commitee should understand and back the proposal.
6. Other rules will be made up when necessary.

## Is this repository a centralizing factor?

To promote interoperability, we standards that everybody can follow, and we need them to define a **single way of doing each thing** without ever hurting **backwards-compatibility**, and for that purpose there is no way around getting everybody to agree on the same thing and keep a centralized index of these standards. However the fact that such index exists doesn't hurt the decentralization of EBSI-ELSA. _At any point the central index can be challenged if it is failing to fulfill the needs of the protocol_ and it can migrate to other places and be maintained by other people.

It can even fork into multiple and then some clients would go one way, others would go another way, and some clients would adhere to both competing standards. This would hurt the simplicity, openness and interoperability of EBSI-ELSA a little, but everything would still work.

There is a list of notable EBSI-ELSA software developers who have commit access to this repository, but that exists mostly for practical reasons, as by the nature of the thing we're dealing with the repository owner can revoke membership and rewrite history as they want -- and if these actions are unjustified or perceived as bad or evil the community must react.

## How this repository works

Standards should emerge from discussion with stakeholders and the steering committee. The committee then defines the upgrade that could benefit multiple clients and the protocol in general without breaking **backwards-compatibility** and the principle of having **a single way of doing things**, then they write that idea and submit it to this repository, other interested parties read it and give their feedback, then once most people reasonably agree we codify that in a ELIP which client developers that are interested in the feature can proceed to implement.

## Breaking Changes

Section reserved for important changes breaking the protocol

## License

All ELIPs are public domain.

## Contributors

 - [Thomas Rossi](https://gitlab.com/S4m0ht)